<?php

/**
 * @file
 * Module.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_embed\Form\EntityEmbedDialog;

/**
 * Implements hook_form_alter().
 */
function paragraphs_inline_entity_form_form_alter(&$form, FormStateInterface $form_state): void {
  /** @var Drupal\Core\Entity\FieldableEntityInterface $entity */
  $formObject = $form_state->getFormObject();
  if ($formObject instanceof EntityFormInterface) {
    $form['#attached']['library'][] = 'paragraphs_inline_entity_form/dialog';
  }

  if ($formObject instanceof EntityEmbedDialog) {
    if (paragraphs_inline_entity_form_entity_browser_is_paragraph_item($form)) {
      $form['#attached']['library'][] = 'paragraphs_inline_entity_form/dialog';
      switch ($form_state->get('step')) {
        case 'select':
          // Do nothing.
          break;

        case 'embed':
          // Change the back button to link to the paragraph edit form.
          $entity_uuid = $form['attributes']['data-entity-uuid']['#value'];
          $entity_type_manager = \Drupal::service('entity_type.manager');
          $entity = $entity_type_manager->getStorage('paragraph')
            ->loadByProperties(['uuid' => $entity_uuid]);
          $paragraph = current($entity);

          $form['actions']['back'] = [
            '#type' => 'submit',
            '#value' => t('Edit paragraph'),
            '#submit' => [],
            '#ajax' => [
              'url' => Url::fromRoute('entity_browser.edit_form', [
                'entity_type' => 'paragraph',
                'entity' => $paragraph->id(),
              ]),
              'event' => 'click',
            ],
          ];
          break;
      }
    }
  }
}

/**
 * Checks that the browser type is a paragraph_item.
 *
 * @param array $form
 *   The form.
 *
 * @return bool
 *   True if it is a paragraph_item entity browser.
 */
function paragraphs_inline_entity_form_entity_browser_is_paragraph_item(array $form): bool {
  if (isset($form['entity_browser']['#entity_browser'])
    && $form['entity_browser']['#entity_browser'] == 'paragraph_items') {
    return TRUE;
  }
  if (isset($form['attributes']['data-embed-button'])
    && $form['attributes']['data-embed-button']['#value'] == 'paragraphs_inline_entity_form') {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_entity_embed_values_alter().
 */
function paragraphs_inline_entity_form_entity_embed_values_alter(array &$values, ContentEntityInterface $entity): void {
  // Add dummy value to trigger a refresh of the embedded content preview.
  $values['attributes']['alt'] = $values['attributes']['data-editor-embed-uuid'];
}
